<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = "Cast";
    protected $fillable = ['id', 'nama', 'umur', 'bio'];

    public function peran()
    {
        return $this->belongsTo('App\Peran');
    }
}
