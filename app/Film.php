<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'film';
    protected $fillable = ['judul', 'ringkasan', 'poster', 'genre_id', 'tahun'];

    public function genre(){
        return $this->belongsTo('App\genre');
    }

    public function peran(){
        return $this->hasMany('App\peran');
    }
}
