<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Kritik;
use App\Film;
use App\User;

class KritikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->only('index', 'show');
    }

    public function index()
    {
        $kritik = Kritik::all();
        return view('kritik.index', compact('kritik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $film = Film::all();
        $user = User::all();
        return view('kritik.create', compact('user','film'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'film_id' => 'required',
            'user_id' => 'required',
            'isi' => 'required',
            'point' => 'required',
    	]);
 
        Kritik::create([
    		'film_id' => $request->film_id,
    		'user_id' => $request->user_id,
    		'isi' => $request->isi,
    		'point' => $request->point
    	]);
 
    	return redirect('/kritik');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kritik = Kritik::find($id);
        return view('kritik.edit', compact('kritik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'isi' => 'required',
            'point' => 'required',
        ]);

        $kritik = Kritik::findorfail($id);
        $kritik->update([
    		'isi' => $request->isi,
    		'point' => $request->point]);
        return redirect('/kritik');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kritik = Kritik::findorfail($id);
        $kritik->delete();

        return redirect('/kritik');
    }
}
