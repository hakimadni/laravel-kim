<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function form()
    {
        return view('register');
    }

    public function sapa(Request $request)
    {
        $nama = $request['nama'];
        return view('welcome', compact('nama'));
    }

    public function table()
    {
        return view('table');
    }

}
