<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    public $timestamps = false;
    protected $table = "peran";
    protected $fillable = ['film_id', 'cast_id', 'nama'];

    public function film()
    {
        return $this->belongsTo('App\film');
    }

    public function cast()
    {
        return $this->belongsTo('App\cast');
    }
}
