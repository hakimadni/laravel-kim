<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public $timestamps = false;
    protected $table = "genre";
    protected $fillable = ['nama'];
    
    public function film(){
        return $this->hasMany('App\Film');
    }
}
