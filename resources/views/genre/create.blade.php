@extends('layout.master')
@section('judul')
    Halaman Tambah Genre
@endsection
@section('isi')
    
        <h2>Tambah Data</h2>
            <form action="/genre" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama Genre</label>
                    <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
@endsection