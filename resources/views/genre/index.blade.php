@extends('layout.master')
@section('judul')
    Halaman List Genre
@endsection
@section('isi')
<a href="/genre/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">ID</th>
        <th scope="col">Nama Tag</th>
        <th scope="col">List Film</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key=>$value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->id}}</td>
                <td>{{$value->nama}}</td>
                <td>
                    <ul>
                        @foreach ($value->film as $item)
                            <li>
                                {{$item->judul}}
                            </li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    
                    <form action="/cast/{{$value->id}}" method="POST">
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection