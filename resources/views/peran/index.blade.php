@extends('layout.master')
@section('judul')
    Halaman List Peran
@endsection
@section('isi')
<a href="/peran/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Cast</th>
        <th scope="col">Film</th>
        <th scope="col">Peran</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($peran as $key=>$value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->cast->nama}}</td>
                <td>{{$value->film->judul}}</td>
                <td>{{$value->nama}}</td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection