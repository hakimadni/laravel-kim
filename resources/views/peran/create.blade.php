@extends('layout.master')
@section('judul')
    Halaman Tambah Genre
@endsection
@section('isi')
    
        <h2>Tambah Data</h2>
            <form action="/peran" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="film">Film</label>
                    <select name="film_id" class="form-control" id="">
                        <option value="">Pilih Film</option>
                        @foreach ($film as $item)
                            <option value="{{$item->id}}">{{$item->judul}}</option>
                        @endforeach
                    </select>
                    @error('genre_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="cast_id">Cast</label>
                    <select name="cast_id" class="form-control" id="">
                        <option value="">Pilih Cast</option>
                        @foreach ($cast as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    @error('genre_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nama">Nama Peran</label>
                    <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
@endsection