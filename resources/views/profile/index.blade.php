@extends('layout.master')
@section('judul')
    Halaman List Genre
@endsection
@section('isi')
<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="">Nama</label>
        <input type="text" class="form-control" id="" disabled value="{{$profile->user->name}}">
    </div>

    <div class="form-group">
        <label for="">Email</label>
        <input type="text" class="form-control" id="" disabled value="{{$profile->user->email}}">
    </div>

    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" id="" value="{{$profile->umur}} ">
        @error('umur')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea name="alamat" class="form-control" id="" cols="30" rows="10">{{$profile->alamat}}</textarea>
        @error('alamat')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection