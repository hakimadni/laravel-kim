@extends('layout.master')
@section('judul')
    Halaman Update film ({{$genre->id}})
@endsection
@section('isi')
    
        <h2>Update Data</h2>
            <form action="/genre/{{$genre->id}} " method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="nama">Nama Genre</label>
                    <input type="text" class="form-control" name="nama" id="title" value="{{$genre->nama}}">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
@endsection