@extends('layout.master')
@section('judul')
    Halaman List film
@endsection
@section('isi')
<a href="/film/create" class="btn btn-primary">Tambah</a>
    <div class="row">
        @foreach ($film as $item)
            <div class="col-4 mt-5">
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('poster/' . $item->poster )}}" class="card-img-top rounded" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$item->judul}} {{$item->tahun}} </h5>
                      <p class="card-text">{{Str::limit($item->ringkasan,50)}} </p>
                      <p><span class="badge badge-dark">{{$item->genre->nama}}</span></p>
                      <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                      <a href="/film/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                    <form action="/film/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                    </div>
                  </div>
            </div>
        @endforeach
    </div>
@endsection