@extends('layout.master')
@section('judul')
    Show Film
@endsection
@section('isi')
<a href="/film" class="btn btn-info">Back</a>
<div class="card mb-3 border mt-5" style="max-width: 80%;">
        <img src="{{asset('poster/' . $film->poster )}}" class="card-img-top rounded" alt="...">
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">{{$film->judul}} {{$film->tahun}}</h5>
          <p class="card-text">{{$film->ringkasan}}</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
    </div>
  </div>

@endsection