@extends('layout.master')
@section('judul')
    Halaman Tambah film
@endsection
@section('isi')
    
        <h2>Tambah Data</h2>
            <form action="/film" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="judul">judul film</label>
                    <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Title">
                    @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="Ringkasan">Ringkasan</label>
                    <br>
                    <textarea name="ringkasan" id="" class="form-control" cols="30" rows="10"></textarea>
                    @error('Ringkasan')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="poster">Poster</label><br>
                    <input type="file" name="poster" id="body">
                    @error('poster')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="genre_id">Genre</label>
                    <select name="genre_id" class="form-control" id="">
                        <option value="">Pilih Genre</option>
                        @foreach ($genre as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    @error('genre_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" class="form-control" name="tahun" id="body">
                    @error('tahun')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
@endsection