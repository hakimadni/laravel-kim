@extends('layout.master')
@section('judul')
    Halaman Update film ({{$film->id}}) {{$film->judul}}
@endsection
@section('isi')
    
        <h2>Update Data</h2>
            <form action="/film/{{$film->id}} " method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="judul">judul film</label>
                    <input type="text" class="form-control" name="judul" id="title" value="{{$film->judul}}">
                    @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="Ringkasan">Ringkasan</label>
                    <br>
                    <textarea name="ringkasan" class="form-control" id="" cols="30" rows="10">{{$film->ringkasan}}</textarea>
                    @error('Ringkasan')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="poster">Poster</label><br>
        <img src="{{asset('poster/' . $film->poster )}}" class="rounded" style="max-width: 60%;" alt="...">
                    <br> <br>
                    <input type="file" name="poster" id="body">
                    @error('poster')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="genre_id">Genre</label>
                    <select name="genre_id" class="form-control" id="">
                        <option value="">Pilih Genre</option>
                        @foreach ($genre as $item)
                            @if ($item->id == $film->genre_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                            @else
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('genre_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" class="form-control" name="tahun" value="{{$film->tahun}}">
                    @error('tahun')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
@endsection