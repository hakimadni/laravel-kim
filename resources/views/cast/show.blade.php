@extends('layout.master')
@section('judul')
    Show Data
@endsection
@section('isi')
<a href="/cast" class="btn btn-info">Back</a>
    <h2>Show Post {{$cast->id}}</h2>
    <br>
    <h4>Nama : {{$cast->nama}}</h4>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio : {{$cast->bio}}</p>
@endsection