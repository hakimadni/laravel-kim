@extends('layout.master')
@section('judul')
    Halaman Tambah Cast
@endsection
@section('isi')
    
        <h2>Tambah Data</h2>
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama Cast</label>
                    <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
                    @error('nama_cast')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" name="umur" id="body">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <br>
                    <textarea name="bio" id="" cols="30" rows="10"></textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
@endsection