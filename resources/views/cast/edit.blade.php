@extends('layout.master')
@section('judul')
    Halaman Edit Cast
@endsection
@section('isi')
    
        <h2>Edit Data {{$cast->id}}</h2>
            <form action="/cast/{{$cast->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="nama">Nama Cast</label>
                    <input type="text" class="form-control" name="nama" id="title" value="{{$cast->nama}}">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" name="umur" id="body" value="{{$cast->umur}}">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <br>
                    <textarea name="bio" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
@endsection