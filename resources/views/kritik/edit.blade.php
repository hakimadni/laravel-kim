@extends('layout.master')
@section('judul')
    Halaman Update film ({{$kritik->id}})
@endsection
@section('isi')
    
<h2>Tambah Data</h2>
<form action="/kritik" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="film_id">Film</label>
        <select name="film_id" class="form-control" id="">
            <option value="" selected disabled>{{$kritik->film->judul}}</option>
        </select>
        @error('file_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="user_id">User</label>
        <select name="user_id" class="form-control" id="">
                <option value="" selected disabled>{{$kritik->user->name}}</option>
        </select>
        @error('user_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="isi">Isi</label>
        <textarea class="form-control" name="isi" id="" cols="30" rows="10">{{$kritik->isi}}</textarea>
        @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="point">Point</label>
        <select name="point" class="form-control" id="">
            <option value="">{{$kritik->point}}</option>
            @for ($i = 1; $i < 10; $i++)
                <option value="{{$i}}">{{$i}}</option>
            @endfor
        </select>
        @error('user_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection