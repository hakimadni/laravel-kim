@extends('layout.master')
@section('judul')
    Halaman List Peran
@endsection
@section('isi')
<a href="/kritik/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">User</th>
        <th scope="col">Film</th>
        <th scope="col">Isi</th>
        <th scope="col">Point</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kritik as $key=>$value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->user->name}}</td>
                <td>{{$value->film->judul}}</td>
                <td>{{$value->isi}}</td>
                <td>{{$value->point}}</td>
                <td>
                    <form action="/kritik/{{$value->id}}" method="POST">
                        <a href="/kritik/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection