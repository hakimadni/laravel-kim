@extends('layout.master')
@section('judul')
    <h1>Buat Account Baru!</h1>
@endsection
    
@section('isi')
<h3>Sign Up Form</h3>
<form method="POST" action="/sapa">
    @csrf
    <br>
    <label for="">First Name:</label>
    <br><br>
    <input type="text" name="nama">
    <br><br>
    <label for="">Last Name:</label>
    <br><br>
    <input type="text">
    <br><br>

    <label>Gender:</label><br><br>
    <input type="radio" name="">
    <label for="">Male</label><br><br>
    <input type="radio" name="">
    <label for="">Female</label><br><br>
    <input type="radio" name="">
    <label for="">Other</label><br><br>
<br><br>
    <p>Nationality</p><br><br>
    <select name="" id="">
        <option value="">Indonesia</option>
        <option value="">Malaysia</option>
    </select>

    <label for="">Language Spoken:</label><br><br>
    <input type="checkbox" name="" id="">
    Bahasa Indonesia <br><br>
    <input type="checkbox" name="" id="">
    English <br><br>
    <input type="checkbox" name="" id="">
    Other <br><br>
    <label for="">Bio:</label> <br><br>
    <textarea name="" id="" cols="30" rows="10"></textarea>
<br>    
<input type="submit">
</form>
@endsection