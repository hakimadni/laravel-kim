<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::group(['middleware' => ['auth']], function () {

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'RegisterController@form');
Route::get('/table', 'RegisterController@table');
Route::post('/sapa', 'RegisterController@sapa');

// Route::get('/data-tables', function(){
//     return view('table.data');
// });

//CRUD Cast
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
});

Route::get('/', function(){
    return view('home');
});


Route::resource('kritik', 'KritikController');
Route::resource('peran', 'PeranController');
Route::resource('profile', 'ProfileController');
Route::resource('film', 'FilmController');
Route::resource('genre', 'GenreController');
Auth::routes();
